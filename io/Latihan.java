package com.d3ti.pbolanjt20.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class Latihan {

	public static void main(String[] args) {
		String line = "";
		String fileContent = "";
		Scanner scanner = new Scanner(System.in);
		System.out.println("Masukkan path file : ");
		String pathFile = scanner.nextLine();
		Path path = Paths.get(pathFile);
		File file = new File(pathFile);
		try {
			file.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Masukkan teks yang ingin ditambahkan : ");
		String tulis = scanner.nextLine();
		
		try {
			BufferedWriter bw = Files.newBufferedWriter(path, 
					Charset.forName("ISO-8859-1"),
					StandardOpenOption.CREATE, 
					StandardOpenOption.APPEND);
			bw.write(tulis,0,tulis.length());
			bw.newLine();
			bw.close();
			BufferedReader fileInput = new BufferedReader(new FileReader(pathFile));
			line = fileInput.readLine();
			fileContent = line + "\n";
			while(line != null) {
				line = fileInput.readLine();
				if(line != null) fileContent += line + "\n";
			}
			fileInput.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Path pathRead=Paths.get(pathFile);
		System.out.print("Nama File             = ");
		System.out.println(pathRead.getFileName());
		System.out.print("Parent File           = ");
		System.out.println(pathRead.getParent());
		System.out.print("Jumlah Subfolder      = ");
		System.out.println(pathRead.getNameCount());
		System.out.print("Path Bertitle Absolut = ");
		System.out.println(pathRead.isAbsolute());
		System.out.print("Subpath dari 0 ke 3   = ");
		System.out.println(pathRead.subpath(0, 1));
		
		System.out.print("Tipe Readable         = ");
		System.out.println(Files.isReadable(path));
		System.out.print("Tipe Writeable        = ");
		System.out.println(Files.isWritable(path));
		System.out.print("Tipe Executable       = ");
		System.out.println(Files.isExecutable(path));
		System.out.print("Tipe Hidden           = ");
		try {
			System.out.println(Files.isHidden(path));
			System.out.print("Apakah Sama Filenya  = ");
			System.out.println(Files.isSameFile(path, pathRead));
			System.out.print("Apakah Filenya Ada   = ");
			System.out.println(Files.exists(path));
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
			} 
	
		System.out.println();
		System.out.println("File berisi 			= ");
		System.out.println(fileContent);

	}

}
