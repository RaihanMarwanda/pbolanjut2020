package com.d3ti.pbolanjt20.io;

import java.util.Scanner;

import com.d3ti.pbolanjt20.innerinterface.Persegi;

public class InputKeyboard {
	public static void main(String b[]) {
		Scanner scanner=new Scanner(System.in);
		System.out.print("Masukkan Sisi : ");
		String sisi_str = scanner.next();
		double sisi_dbl = Double.valueOf(sisi_str);
		Persegi persegi = new Persegi(sisi_dbl);
		System.out.println("-----------");
		System.out.println("Sisi persegi = "+sisi_str);
		System.out.println("Luas = "+persegi.getLuas());
		System.out.println("Keliling = "+persegi.getKeliling());
	}

}
