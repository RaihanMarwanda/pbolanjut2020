package com.d3ti.pbolanjt20.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BacaFile  {
	public static void main (String[] bc) {
		String letakFile="D:\\D3 TI UNS 2018\\Semestes 4\\Pemograman Berbasis Obyek Lanjut\\Praktikum\\prak.io\\HelloWorld.txt";
		String line="";
		String fileContent="";
		
		try {
			BufferedReader fileInput=new BufferedReader(
					new FileReader(new File(letakFile)));
			line=fileInput.readLine();
			fileContent=line;
			while(line !=null) {
				line =fileInput.readLine();
				if(line !=null) fileContent+=fileContent;
			}
			
			fileInput.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("File tidak ditemukan");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("File tidak bisa dibaca");
		}
		System.out.println(fileContent);
		Path path=Paths.get(letakFile);
		System.out.print("Nama File             = ");
		System.out.println(path.getFileName());
		System.out.print("Parent File           = ");
		System.out.println(path.getParent());
		System.out.print("Jumlah Subfolder      = ");
		System.out.println(path.getNameCount());
		System.out.print("Path Bertitle Absolut = ");
		System.out.println(path.isAbsolute());
		System.out.print("Subpath dari 0 ke 3   = ");
		System.out.println(path.subpath(0, 3));
		
		System.out.print("Apakah Readable       = ");
		System.out.println(Files.isReadable(path));
		System.out.print("Apakah Writeable      = ");
		System.out.println(Files.isWritable(path));
		System.out.print("Apakah Executable     = ");
		System.out.println(Files.isExecutable(path));

		
		
	}

}
