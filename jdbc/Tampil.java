import java.awt.BorderLayout;
import java.awt.EventQueue;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Tampil extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	DBBiodata biodata;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tampil frame = new Tampil();
					frame.setVisible(true);
					 System.out.println("frame berhasil;");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tampil() {
		super("Biodata");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		biodata = new DBBiodata();
		@SuppressWarnings("rawtypes")
		Vector columnNames = new Vector();
		columnNames.addElement("NIM");
		columnNames.addElement("Nama");
		columnNames.addElement("Jenis Kelamin");
		columnNames.addElement("Tempat Lahir");
		columnNames.addElement("Tanggal Lahir");
		columnNames.addElement("Alamat");
		JTable table = new JTable();
		DefaultTableModel dtm=new DefaultTableModel(biodata.data(),columnNames);
		table.setModel(dtm);
		scrollPane.setColumnHeaderView(table);

	}

}