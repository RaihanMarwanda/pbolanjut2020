public class PersegiPanjang extends BangunDatar {
    float panjang;
    float lebar;
    
    @Override
    float luas(){
        final float luas = panjang * lebar;
        System.out.println("Luas Persegi Panjang:" + luas);
        return luas;
    }

    @Override
    float keliling() {
        final float kll = 2 * panjang + 2 * lebar;
        System.out.println("Keliling Persegi Panjang: " + kll);
        return kll;
    }
}

