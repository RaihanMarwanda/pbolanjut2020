import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;
import java.awt.*;

	public class Swing2 extends JFrame implements ActionListener,ChangeListener{
		ButtonGroup bg;
		JRadioButton rb1,rb2;
		JComboBox cb;
		JButton b;
		final String[] jenis = {"Laptop","Harddisk","Mouse"};
		JMenuBar mb;
		JMenu m;
		double diskon;
		JTextField txAngka;
		JLabel lbAngka;
		JPanel panel1;
		JFrame frame;
		int harga;
		double total;
		
		public Swing2(){
		
			super("Radio Combo");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			Container cp = this.getContentPane();
			cp.setLayout(new FlowLayout());
			Panel p = new Panel();
			add(p);
			p.setLayout(new GridLayout(9,10));
			lbAngka = new JLabel("Masukkan Angka :");
			txAngka = new JTextField();
			panel1 = new JPanel();
			p.add(lbAngka);
        	p.add(txAngka);
			mb = new JMenuBar();
			mb.add(m=new JMenu("exit"));
			m.addChangeListener((ChangeListener) this);;
			this.setJMenuBar(mb);
			p.add(rb1 = new JRadioButton("MEMBER"));
			p.add(rb2 = new JRadioButton("NON MEMBER"));
			bg = new ButtonGroup();
			bg.add(rb1);
			bg.add(rb2);
			cp.add(cb =new JComboBox(jenis));
			
			cp.add(b = new JButton("save"));
			this.setVisible(true); 
			b.addActionListener((ActionListener) this);
		}

		public static void main(String args[]){
			new Swing2();
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getSource() == b){
				String radio=null;
				
				if(rb1.isSelected() == true){
					radio = rb1.getText();
					diskon=0.01;
				}
				else {
					radio = rb2.getText();
					diskon=0;
				}
				if(cb.getSelectedItem().equals("Laptop")){
					harga=8000000;
					
				}
				if(cb.getSelectedItem().equals("Harddisk")){
					harga=200000;
					
				}
				if(cb.getSelectedItem().equals("Mouse")){
					harga=95000;
					
				}
				JFrame frame = new JFrame();
				frame.setTitle("STRUK");
		frame.setSize(400, 300);
		
		Integer angka=Integer.parseInt(txAngka.getText());
				total=(angka*harga)-(angka*diskon*harga);
				System.out.println("Radio Button Memilih: "+radio);
				String combo = cb.getSelectedItem().toString();
				System.out.println("ComboBox Memilih: "+combo);
				System.out.println("Jumlah: "+angka);
				System.out.println("Harga awal: "+angka*harga);
				System.out.println("Diskon: "+diskon);
				System.out.println("Total: "+(total));

		frame.setLocationRelativeTo(null);
		JLabel label = new JLabel("Keanggotaan = "+radio);
		JLabel label2 = new JLabel("Barang = "+combo);
		JLabel label3 = new JLabel("Jumlah= "+angka);
		JLabel label4 = new JLabel("Harga awal= "+angka*harga);
		JLabel label5 = new JLabel("Diskon= "+angka*harga*diskon);
		JLabel label6 = new JLabel("Total= "+total);
		
		frame.setLayout(null);
		frame.add(label);
		frame.add(label2);
		frame.add(label3);
		frame.add(label4);
		frame.add(label5);
		frame.add(label6);
		label.setBounds(20, 20, 150, 30);
		label2.setBounds(20, 40, 150, 30);
		label3.setBounds(20, 60, 150, 30);
		label4.setBounds(20, 80, 150, 30);
		label5.setBounds(20, 100, 150, 30);
		label6.setBounds(20, 120, 150, 30);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setResizable(false);
		
		frame.setVisible(true);		
			}
		}
	
		@Override
		public void stateChanged(ChangeEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getSource() == m){
				System.exit(0);
			}
		}
	}
