package com.model.mhs;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.sql.Connection;
import java.sql.Statement;

import com.mysql.jdbc.*;

public class DBBiodata {
	Connection con;
	Statement st;
	ResultSet rs;

	public DBBiodata() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/jdbc?user=root&password=");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Vector data() {
		try {
			st = (Statement) con.createStatement();
			rs = (ResultSet) st.executeQuery("SELECT * FROM datadiri");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Vector dBdata = new Vector(1);
		try {
			while (rs.next()) {
				Vector rows = new Vector();
				rows.add(rs.getString(1));
				rows.add(rs.getString(2));
				rows.add(rs.getString(3));
				rows.add(rs.getString(4));
				rows.add(rs.getString(5));
				rows.add(rs.getString(6));
				dBdata.addElement(rows);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dBdata;
	}

	public void insertBiodata(String nim, String nama, String jk, String tmp_lahir, String tgl_lahir, String alamat) {
		try {
			st = (Statement) con.createStatement();
			st.executeUpdate("INSERT INTO datadiri VALUES" + "(" + "'" + nim + "'," + "'" + nama + "'," + "'" + jk + "',"
					+ "'" + tmp_lahir + "'," + "'" + tgl_lahir + "'," + "'" + alamat + "'" + ")");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void editBiodata(String nim, String nama, String jk, String tmp_lahir, String tgl_lahir, String alamat) {
		try {
			st = (Statement) con.createStatement();
			st.executeUpdate("UPDATE datadiri SET nama='" + nama + "', jk='" + jk + "', tmp_lahir='" + tmp_lahir + "', tgl_lahir='"
							+ tgl_lahir + "', alamat='" + alamat + "' WHERE nim='" + nim + "'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
