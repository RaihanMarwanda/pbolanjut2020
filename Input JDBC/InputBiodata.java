package com.d3ti.pbolanjt.jdbc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;


public class InputBiodata {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InputBiodata window = new InputBiodata();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InputBiodata() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 730, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("111px"),
				FormSpecs.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("116px"),},
			new RowSpec[] {
				RowSpec.decode("48px"),
				FormSpecs.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("48px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		textField = new JTextField();
		textField.setBounds(128, 28, 86, 20);
		frame.getContentPane().add(textField, "3, 1, left, bottom");
		textField.setColumns(10);
		
		JLabel nim = new JLabel("NIM");
		nim.setBounds(65, 31, 46, 14);
		frame.getContentPane().add(nim, "1, 1, right, bottom");
		
		JLabel nama = new JLabel("Nama");
	    nama.setBounds(65, 68, 46, 14);
	    frame.getContentPane().add(nama, "1, 3, right, center");
	    
	    textField_1 = new JTextField();
	    textField_1.setBounds(128, 65, 86, 20);
	    frame.getContentPane().add(textField_1, "3, 3, left, fill");
	    textField_1.setColumns(10);
	    
	    
	    
	    JLabel jk = new JLabel("Jenis Kelamin");
	    jk.setBounds(65, 105, 46, 14);
	    frame.getContentPane().add(jk, "1, 5, right, center");
	    JComboBox<String> comboBox = new JComboBox<String>();
	    comboBox.addItem("Laki-Laki");
	    comboBox.addItem("Perempuan");
	    comboBox.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        }
	    });
	    comboBox.setBounds(128, 102, 86, 20);
	    frame.getContentPane().add(comboBox, "3, 5, left, fill");
	    
	    JLabel tmp_lahir = new JLabel("Tempat Lahir");
	    tmp_lahir.setBounds(65, 142, 46, 14);
	    frame.getContentPane().add(tmp_lahir, "1, 7, right, center");
	    
	    textField_3 = new JTextField();
	    textField_3.setBounds(128, 139, 86, 20);
	    frame.getContentPane().add(textField_3, "3, 7, left, fill");
	    textField_3.setColumns(10);
	    
	    JLabel tgl_lahir = new JLabel("Tanggal Lahir");
	    tgl_lahir.setBounds(65, 179, 46, 14);
	    frame.getContentPane().add(tgl_lahir, "1, 9, right, center");
		
	    textField_4 = new JTextField();
	    textField_4.setBounds(128, 176, 86, 20);
	    frame.getContentPane().add(textField_4, "3, 9, left, fill");
	    textField_4.setColumns(10);
	    
	    JLabel alamat = new JLabel("Alamat");
	    alamat.setBounds(65, 216, 46, 14);
	    frame.getContentPane().add(alamat, "1, 11, right, top");
	    
	    JTextArea textArea_1 = new JTextArea();
	    textArea_1.setBounds(128, 213, 86, 50);
	    frame.getContentPane().add(textArea_1, "3, 11, fill, fill");
	    
	    JButton simpan = new JButton("Simpan");
	    simpan.setBounds(65, 253, 46, 14);
	    frame.getContentPane().add(simpan, "3, 13, fill, fill");

	    
	    simpan.addActionListener(new ActionListener() {    
	    	public void actionPerformed(ActionEvent e) {     
	    		String nim=textField.getText();     
	    		String nama=textField_1.getText();     
	    		String jk=comboBox.getSelectedItem().toString();     
	    		String tmp_lahir=textField_3.getText();     
	    		String tgl_lahir=textField_4.getText(); 
	    		String alamat=textArea_1.getText();      
	    		DBBiodata biodata = new DBBiodata();     
	    		biodata.insertBiodata(nim,            
	    				nama,           
	    				jk,            
	    				tmp_lahir,            
	    				tgl_lahir,           
	    				alamat);
            }
        });
            }
        ;	
	}

