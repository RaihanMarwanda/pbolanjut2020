package com.d3ti.pbolanjt20.innerinterface;

public class  Segitiga {

	static BidangDatar segitiga = new BidangDatar() {
		double alas = 3;
		double tinggi = 4;
		double sisi_miring = 5;
		
		@Override
		public double getLuas() {
			return (alas * tinggi) / 2;
		}
		
		@Override
		public double getKeliling() {
			return alas + tinggi + sisi_miring;
		}
	};
	
	public static void main(String[] args) {
		System.out.println("Luas Segitiga 	  : " + segitiga.getLuas());
		System.out.println("Keliling Segitiga : " + segitiga.getKeliling());
	}
}
