package com.d3ti.pbolanjt20.innerinterface;

public class KonversiSuhu implements TransformasiSuhu {

	@Override
	public double CelciustoFarenhit(double celcius) {
		// TODO Auto-generated method stub
		return ((9* celcius)/5)+32;
	}

	@Override
	public double CelciustoReamur(double celcius) {
		// TODO Auto-generated method stub
		return (4*celcius)/5;
	}
	
	@Override
	public double FarenhittoCelcius(double farenhit) {
		// TODO Auto-generated method stub
		return ((farenhit-32)*5)/9;
	}

	@Override
	public double FarenhittoReamur(double farenhit) {
		// TODO Auto-generated method stub
		return ((farenhit-32)*4)/9;
	}

	@Override
	public double ReamurtoCelcius(double reamur) {
		// TODO Auto-generated method stub
		return ((5*reamur)/4);
	}

	@Override
	public double ReamurtoFarenhit(double reamur) {
		// TODO Auto-generated method stub
		return ((9*reamur)/4)+32;
	}

	
	public static void main(String[] args) {
		KonversiSuhu A = new KonversiSuhu();
		KonversiSuhu B = new KonversiSuhu();
		KonversiSuhu C = new KonversiSuhu();
		System.out.println("30 Celcius ke Farenhit : " + A.CelciustoFarenhit(30));
		System.out.println("30 Celcius ke Reamur : " + A.CelciustoReamur(30));
		System.out.println("128 Farenhit ke Celcius : " + B.FarenhittoCelcius(68));
		System.out.println("128 Farenhit ke Reamur : " + B.FarenhittoReamur(68));
		System.out.println("40 Reamur ke Celcius : " + C.ReamurtoCelcius(40));
		System.out.println("40 Reamur ke Farenhit : " + C.ReamurtoFarenhit(40));


	}
	

}
