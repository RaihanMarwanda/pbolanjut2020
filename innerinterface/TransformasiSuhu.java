package com.d3ti.pbolanjt20.innerinterface;

public interface TransformasiSuhu {
	final int FARENHIT=32;
	
	public double CelciustoFarenhit(double celcius);
	public double CelciustoReamur(double celcius);
	
	public double FarenhittoCelcius(double farenhit);
	public double FarenhittoReamur(double farenhit);
	
	public double ReamurtoCelcius(double reamur);
	public double ReamurtoFarenhit(double reamur);
	
	

}

