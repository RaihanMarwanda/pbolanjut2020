package com.d3ti.pbolanjt20.innerinterface;

public interface BidangDatar {
	public abstract double getLuas();
	public abstract double getKeliling();

}
