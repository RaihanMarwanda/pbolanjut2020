package com.d3ti.pbolanjt20.innerinterface;

public class Jarak {
	 static class KonversiJarak{
		double besar=0;
		int awal=0;
		int akhir=0;
		double pengali=1;
		public KonversiJarak (String awal,
						 	  String akhir,
						      double besar) {
			this.awal=konversiSatuan(awal);
			this.akhir=konversiSatuan(akhir);
			this.besar=besar;
			
		}
		double getSelisih() {
			int selisih=0;
			selisih=awal-akhir;
			if(selisih >0) {
				for(int i=0; i<selisih; i++) {
					pengali=pengali/10;
				}
			}
			else if (selisih <0) {
					for(int i=selisih; i<0; i++) {
						pengali=pengali*10;
					}
				}
				return pengali;
		}
		
		double hasil() {
			return getSelisih()*besar;
		}
	}
		  static int konversiSatuan(String satuan) {
			switch(satuan){
				case"km" 	: return 1;
				case"hm" 	: return 2;
				case"dam" 	: return 3;
				case"m" 	: return 4;
				case"dm" 	: return 5;
				case"cm" 	: return 6;
				case"mm" 	: return 7;
				default 	: return 0;
			}
		}
		
		public static void main(String a[]) {
				KonversiJarak jarak=new KonversiJarak("km","m",19);
				System.out.println("Hasil Konversi "+jarak.hasil());
		}	
}
	

