package com.d3ti.pbolanjt20.innerinterface;

public class Persegi implements BidangDatar{
		double sisi;
		
		public Persegi(double sisi) {
			this.sisi = sisi;
		}
		@Override
		public double getLuas() {
			return sisi*sisi;
		}

		@Override
		public double getKeliling() {
			return 4*sisi;
		}

		public static void main(String[] args) {
			Persegi persegi = new Persegi(5);
			System.out.println("Hasil Luas : "+persegi.getLuas());
			System.out.println("Hasil Keliling : "+persegi.getKeliling());
		}


	}