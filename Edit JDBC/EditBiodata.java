package com.d3ti.pbolanjt.jdbc;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.Button;

public class EditBiodata extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditBiodata frame = new EditBiodata();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditBiodata() {
		setTitle("Edit Biodata");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(7, 2, 0, 0));
		
		JLabel lblNim = new JLabel("NIM");
		contentPane.add(lblNim);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNama = new JLabel("Nama");
		contentPane.add(lblNama);
		
		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblJenisKelamin = new JLabel("Jenis Kelamin");
		contentPane.add(lblJenisKelamin);
		
		JComboBox comboBox = new JComboBox();
		contentPane.add(comboBox);
		comboBox.addItem("Laki-Laki");
	    comboBox.addItem("Perempuan");
	    comboBox.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        }
	    });
	    
	    JLabel lblTempatLahir = new JLabel("Tempat Lahir");
	    contentPane.add(lblTempatLahir);
	    
	    textField_2 = new JTextField();
	    contentPane.add(textField_2);
	    textField_2.setColumns(10);
	    
	    JLabel lblTanggalLahir = new JLabel("Tanggal Lahir");
	    contentPane.add(lblTanggalLahir);
	    
	    textField_3 = new JTextField();
	    contentPane.add(textField_3);
	    textField_3.setColumns(10);
	    
	    JLabel lblAlamat = new JLabel("Alamat");
	    contentPane.add(lblAlamat);
	    
	    JTextArea textArea = new JTextArea();
	    contentPane.add(textArea);
	    
	    JButton btnEdit = new JButton("Edit");
	    btnEdit.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		String nim=textField.getText();     
	    		String nama=textField_1.getText();     
	    		String jk=comboBox.getSelectedItem().toString();     
	    		String tmp_lahir=textField_2.getText();     
	    		String tgl_lahir=textField_3.getText(); 
	    		String alamat=textArea.getText();      
	    		DBBiodata biodata = new DBBiodata();     
	    		biodata.editBiodata(nim,            
	    				nama,           
	    				jk,            
	    				tmp_lahir,            
	    				tgl_lahir,           
	    				alamat);
	    	}
	    });
	    contentPane.add(btnEdit);
	}

}
